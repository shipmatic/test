from flask import Flask
from faker import Faker
from flask import jsonify

app = Flask(__name__)



@app.route('/')
def hello():
    return('Hello')

@app.route('/random_user')
def get_random_user():
    fake = Faker()
    a = []
    for i in range (100):
        a.append( fake.company_email() +' '+ fake.name())
    
    return jsonify(a)



@app.route('/requirements')
def get_requirements():
    """ f = open('requirements.txt', 'r')
    x = f.read()
    return(x)
    f.close()
     """
    import csv
    with open('requirements.txt', 'r') as f:
        


@app.route('/avr_data')
def get_avr_data():
    import csv
    with open('hw.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        summ = 0
        a = []
        b = []
        for row in reader:
            a.append(row[" Height(Inches)"])
            b.append(row[" Weight(Pounds)"])
        for i in range(len(a)):
            a[i]=float(a[i].strip())
            b[i]=float(b[i].strip())
    return(str((((sum(a))/len(a)), ((sum(b))/len(b)))))

    

if __name__ == '__main__':

    app.run(debug=True)
